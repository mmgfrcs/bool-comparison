# bool-comparison
[![pipeline status](https://gitlab.com/mmgfrcs/bool-comparison/badges/master/pipeline.svg)](https://gitlab.com/mmgfrcs/bool-comparison/-/commits/master) 
[![coverage report](https://gitlab.com/mmgfrcs/bool-comparison/badges/master/coverage.svg)](https://gitlab.com/mmgfrcs/bool-comparison/-/commits/master)
[![npm bundle size](https://img.shields.io/bundlephobia/min/bool-comparison?label=size&logo=npm)](https://gitlab.com/mmgfrcs/bool-comparison/)
[![npm version](https://img.shields.io/npm/v/bool-comparison?label=ver&logo=npm)](https://www.npmjs.com/package/bool-comparison/)

Provides simple short-hands for common comparisons

## Requirements
- JavaScript ES6 for the use of rest parameters

## Install
Install this package like any other packages:

    npm install bool-comparison

## Usage
Import this package like any other packages:

    const compare = require('bool-comparison')

This package contains documentation. You can check all functions and parameters' description in your IDE.

### Compound Comparison
This package provide compound comparison functions that allow you to write less code to do same-variable comparisons

Each function in this category have the same signature:

    x(value, strict, ...comp)

Where:
- `x` is the name of the function
- `value` is the value to compare
- `comp` are all values to compare `value` with
- `strict` sets whether the comparisons are performed strictly (using `===`) or not (using `==`)

All of these functions compare all values in `comp` against `value` and returns `true` or `false` depending on the function.

#### `compare.all(value, strict, ...comp)`
Returns true only if **all** values in `comp` are equal to `value`

Internally, this function is equal to `value ==(=) comp[0] && value ==(=) comp[1] && value ==(=) comp[2]...`

#### `compare.some(value, strict, ...comp)`
Returns true if **any** values in `comp` are equal to `value`

Internally, this function is equal to `value ==(=) comp[0] || value ==(=) comp[1] || value ==(=) comp[2]...`

#### `compare.none(value, strict, ...comp)`
Returns true only if **all** values in `comp` are *not* equal to `value`

It is the negated form of `compare.all`

#### `compare.notSome(value, strict, ...comp)`
Returns true only if **any** values in `comp` are *not* equal to `value`

It is the negated form of `compare.some`

### Numerical Comparison
This package provide functions to compare numerical data

#### `compare.between(value, max, min, options)`

*Under construction*

## License
[MIT](LICENSE.md)