const assert = require('assert');
const lib = require('../index');

describe('Binary ALL Comparisons (Strict)', function () {
    // Same type
    describe('Same Type Comparisons', function () {
        describe('ALL(0, 0)', function () {
            it('should return true', function () {
                assert.equal(lib.all(0, true, 0), true);
            });
        });
        describe('ALL(0, 1)', function () {
            it('should return false', function () {
                assert.equal(lib.all(0, true, 1), false);
            });
        });
        describe('ALL(1, 1)', function () {
            it('should return true', function () {
                assert.equal(lib.all(1, true, 1), true);
            });
        });
    });

    // Different type but equivalent
    describe('Different Type Comparisons', function () {
        describe('ALL(0, "0")', function () {
            it('should return false', function () {
                assert.equal(lib.all(0, true, '0'), false);
            });
        });
        describe('ALL(0, "1")', function () {
            it('should return false', function () {
                assert.equal(lib.all(0, true, '1'), false);
            });
        });
        describe('ALL(1, "1")', function () {
            it('should return false', function () {
                assert.equal(lib.all(1, true, '1'), false);
            });
        });
    });
});

describe('Binary ALL Comparisons (Lenient)', function () {
    // Same type
    describe('Same Type Comparisons', function () {
        describe('ALL(0, 0)', function () {
            it('should return true', function () {
                assert.equal(lib.all(0, false, 0), true);
            });
        });
        describe('ALL(0, 1)', function () {
            it('should return false', function () {
                assert.equal(lib.all(0, false, 1), false);
            });
        });
        describe('ALL(1, 1)', function () {
            it('should return true', function () {
                assert.equal(lib.all(1, false, 1), true);
            });
        });
    });

    // Different type but equivalent
    describe('Different Type Comparisons', function () {
        describe('ALL(0, "0")', function () {
            it('should return true', function () {
                assert.equal(lib.all(0, false, '0'), true);
            });
        });
        describe('ALL(0, "1")', function () {
            it('should return false', function () {
                assert.equal(lib.all(0, false, '1'), false);
            });
        });
        describe('ALL(1, "1")', function () {
            it('should return true', function () {
                assert.equal(lib.all(1, false, '1'), true);
            });
        });
    });
});
