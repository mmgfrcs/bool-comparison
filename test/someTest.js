const assert = require('assert');
const lib = require('../index');

describe('Binary SOME Comparisons (Strict)', function () {
    // Same type
    describe('Same Type Comparisons', function () {
        describe('SOME(0, 0)', function () {
            it('should return true', function () {
                assert.equal(lib.some(0, true, 0), true);
            });
        });
        describe('SOME(0, 1)', function () {
            it('should return false', function () {
                assert.equal(lib.some(0, true, 1), false);
            });
        });
        describe('SOME(1, 1)', function () {
            it('should return true', function () {
                assert.equal(lib.some(1, true, 1), true);
            });
        });
    });

    // Different type but equivalent
    describe('Different Type Comparisons', function () {
        describe('SOME(0, "0")', function () {
            it('should return false', function () {
                assert.equal(lib.some(0, true, '0'), false);
            });
        });
        describe('SOME(0, "1")', function () {
            it('should return false', function () {
                assert.equal(lib.some(0, true, '1'), false);
            });
        });
        describe('SOME(1, "1")', function () {
            it('should return false', function () {
                assert.equal(lib.some(1, true, '1'), false);
            });
        });
    });
});

describe('Binary SOME Comparisons (Lenient)', function () {
    // Same type
    describe('Same Type Comparisons', function () {
        describe('SOME(0, 0)', function () {
            it('should return true', function () {
                assert.equal(lib.some(0, false, 0), true);
            });
        });
        describe('SOME(0, 1)', function () {
            it('should return false', function () {
                assert.equal(lib.some(0, false, 1), false);
            });
        });
        describe('SOME(1, 1)', function () {
            it('should return true', function () {
                assert.equal(lib.some(1, false, 1), true);
            });
        });
    });

    // Different type but equivalent
    describe('Different Type Comparisons', function () {
        describe('SOME(0, "0")', function () {
            it('should return true', function () {
                assert.equal(lib.some(0, false, '0'), true);
            });
        });
        describe('SOME(0, "1")', function () {
            it('should return false', function () {
                assert.equal(lib.some(0, false, '1'), false);
            });
        });
        describe('SOME(1, "1")', function () {
            it('should return true', function () {
                assert.equal(lib.some(1, false, '1'), true);
            });
        });
    });
});
