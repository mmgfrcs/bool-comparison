const assert = require('assert');
const lib = require('../index');

describe('BETWEEN(5, 0, 10, (1), (2))', function () {
    it('should return true for all combinations of (1) and (2)', function () {
        assert.equal(lib.between(5, 0, 10, true, true), true, 'Returned false for (1) = true and (2) = true');
        assert.equal(lib.between(5, 0, 10, false, true), true, 'Returned false for (1) = false and (2) = true');
        assert.equal(lib.between(5, 0, 10, true, false), true, 'Returned false for (1) = true and (2) = false');
        assert.equal(lib.between(5, 0, 10, false, false), true, 'Returned false for (1) = false and (2) = false');
    });
});

describe('BETWEEN Comparisons (All Exclusive)', function () {
    describe('BETWEEN(0, 0, 1, true, true)', function () {
        it('should return false', function () {
            assert.equal(lib.between(0, 0, 1, { exclusiveMin: true, exclusiveMax: true }), false);
        });
    });
    describe('BETWEEN(1, 0, 1, true, true)', function () {
        it('should return false', function () {
            assert.equal(lib.between(1, 0, 1, { exclusiveMin: true, exclusiveMax: true }), false);
        });
    });
});

describe('BETWEEN Comparisons (Max Exclusive)', function () {
    describe('BETWEEN(0, 0, 1, false, true)', function () {
        it('should return true', function () {
            assert.equal(lib.between(0, 0, 1, { exclusiveMin: false, exclusiveMax: true }), true);
        });
    });
    describe('BETWEEN(1, 0, 1, false, true)', function () {
        it('should return false', function () {
            assert.equal(lib.between(1, 0, 1, { exclusiveMin: false, exclusiveMax: true }), false);
        });
    });
});

describe('BETWEEN Comparisons (Min Exclusive)', function () {
    describe('BETWEEN(0, 0, 1, true, false)', function () {
        it('should return false', function () {
            assert.equal(lib.between(0, 0, 1, { exclusiveMin: true, exclusiveMax: false }), false);
        });
    });
    describe('BETWEEN(1, 0, 1, true, false)', function () {
        it('should return true', function () {
            assert.equal(lib.between(1, 0, 1, { exclusiveMin: true, exclusiveMax: false }), true);
        });
    });
});

describe('BETWEEN Comparisons (All Inclusive)', function () {
    describe('BETWEEN(0, 0, 1, false, false)', function () {
        it('should return true', function () {
            assert.equal(lib.between(0, 0, 1, { exclusiveMin: false, exclusiveMax: false }), true);
        });
    });
    describe('BETWEEN(1, 0, 1, false, false)', function () {
        it('should return true', function () {
            assert.equal(lib.between(1, 0, 1, { exclusiveMin: false, exclusiveMax: false }), true);
        });
    });

    describe('BETWEEN(2, 0, 1, false, false)', function () {
        it('should return false', function () {
            assert.equal(lib.between(2, 0, 1, { exclusiveMin: false, exclusiveMax: false }), false);
        });
    });
});
