module.exports = {
    /**
     * Options for the between function
     * @typedef {Object} BetweenOptions
     * @property {boolean} exclusiveMin Whether min is included in the comparison or not. If true, minimum is not included.
     * @property {boolean} exclusiveMax Whether max is included in the comparison or not. If true, maximum is not included.
     */

    /** Compare all arguments against value. Return true if all arguments are equal to value
     * @param {any} value Value to compare all arguments against.
     * @param {boolean} strict Whether the comparison performed is strict (using ===) or lenient (using ==).
     * @param {Array<any>} comp Values to compare with
     */
    all: function (value, strict, ...comp) {
        return compare({
            value: value,
            strict: strict,
            allOrNothing: true
        }, comp);
    },
    /**
     * Compare all arguments against value. Return true if any arguments are equal to value
     * @param {any} value Value to compare all arguments against.
     * @param {boolean} strict Whether the comparison performed is strict (using ===) or lenient (using ==). Defaults to true
     * @param {Array<any>} comp Values to compare with
     */
    some: function (value, strict, ...comp) {
        return compare({
            value: value,
            strict: strict,
            allOrNothing: false
        }, comp);
    },
    /**
     * Compare all arguments against value. Return true if all arguments are not equal to value
     * @param {any} value Value to compare all arguments against.
     * @param {boolean} strict Whether the comparison performed is strict (using ===) or lenient (using ==). Defaults to true
     * @param {Array<any>} comp Values to compare with
     */
    none: function (value, strict, ...comp) {
        return !this.all(value, strict, comp);
    },
    /**
     * Compare all arguments against value. Return true if any arguments are not equal to value
     * @param {any} value Value to compare all arguments against.
     * @param {boolean} strict Whether the comparison performed is strict (using ===) or lenient (using ==). Defaults to true
     * @param {Array<any>} comp Values to compare with
     */
    notSome: function (value, strict, ...comp) {
        return !this.some(value, strict, comp);
    },
    /**
     * Get whether value is between min and max. Min and max can be inclusive or exclusive individually
     * @param {number} value The value to compare
     * @param {number} min The minimum value
     * @param {number} max The maximum value
     * @param {BetweenOptions} [option] Options for the function
     */
    between: function (value, min, max, option) {
        if (option.exclusiveMin && option.exclusiveMax) {
            if (value > min && value < max) return true;
            else return false;
        } else if (option.exclusiveMin) {
            if (value > min && value <= max) return true;
            else return false;
        } else if (option.exclusiveMax) {
            if (value >= min && value < max) return true;
            else return false;
        } else if (value >= min && value <= max) return true;
        else return false;
    }
};

var compare = function (options, args) {
    // Compare all arguments.
    for (var i = 0; i < args.length; i++) {
        if (options.allOrNothing === true) {
            if (options.strict === true) {
                if (options.value !== args[i]) return false;
            } else if (options.value != args[i]) return false;
        } else {
            if (options.strict === true) {
                if (options.value === args[i]) return true;
            } else if (options.value == args[i]) return true;
        }
    }
    if (options.allOrNothing) return true;
    else return false;
};
