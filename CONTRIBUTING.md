# Contribute to this Repository
*Last update: 2020-03-10, v0.0.0*

First off, thank you for taking the time to contribute to this package!

The following is a guideline on how to contribute to this repository. While I encourage everyone to follow the following guidelines, these are merely guidelines and not hard rules. Use your best judgement and feel free to propose changes.

## Introduction
There are 3 kinds of contributions
- Bugs: Bug reporting
- Features: suggestions for new features
- Code: contribute code directly

Each type of contributions have their own section. Make sure to read them.

## Bug Reporting
### What is a Bug
Bugs are, in their nature, an unexpected behavior in code. Expected behavior that do not conform to your specifications are not bugs; they should be submitted as a suggestion instead. In addition to that, if any of the conditions below are true, it is a bug:
- It causes your program to crash, and the crash come from this package
- Given non-erroneous input, the output is erroneous
-  The described function is inconsistent with the actual function.

### Where to Submit a Bug Report
Bug reports should be submitted on the [issues](https://gitlab.com/mmgfrcs/bool-comparison/-/issues) page of the repository. While other means of bug reporting is possible, reporting it on that page is recommended.

### Before Submitting a Bug Report
- You should find out if the bug has been reported through searching the [issue board](https://gitlab.com/mmgfrcs/bool-comparison/-/boards/1590506) or searching the list of issues. Add a comment on that issue if:
    - The issue is still open
    - The issue is not on the To-do or In Progress list
- If you find a **Closed** issue that seems like the bug you're experiencing, open a new issue and include a link to the original issue in the body of your new issue.
- Check whether the bug has been fixed on future versions.

### Bug Report Guidelines
Here is the list of what you would put on the issue:
1. **Clear, descriptive Title** that identifies the bug in TL;DR fashion. Title should be prepended with *\[Bug\]*, if not a bug label, to clearly differentiate it from other types of contributions
2. **Short, concise Bug Summary** that describes the bug in detail. Provide as much information as possible so the bug can be identified and/or reproduced. A code using the package should be included in the summary if it is a bug in code.
3. **Expectation and Observation** of the summary's result. Provide the inputs, the expected output and the observed output.
4. **Reproduction Steps** if it wasn't clear in the summary. Explain how did you get to the observed result.
5. **Configurations** if it wasn't included in the summary, including, but not limited to, OS, other installed packages and package version
5. **Suggestions**, if any, to resolve the bug. This can make bug fixing faster to do.

## Suggestions
### What it's for
Suggestions are submitted for
- Completely new features relating to the package
- Improvements to existing functionality
- Changes to existing functionality, naming conventions or styles

### Before Submitting a Suggestion
- Same as bugs, you should find out if the suggestion has been submitted through searching the [issue board](https://gitlab.com/mmgfrcs/bool-comparison/-/boards/1591500) or searching the list of issues. Add a comment on that suggestion if:
    - The suggestion is still open
    - The suggestion is not on the To-do or In Progress list
- A **Closed** suggestion usually should not be revisited. However, if you find the suggestion relevant to re-suggest, open a new one and include a link to the original suggestion in the body of your new suggestion.

### Suggestion Guidelines
Here is the list of what you would put on the suggestion:
1. **Clear, descriptive Title** that summarizes the suggestion in a TL;DR fashion. Title should be prepended with *\[Suggestion\]*, if not an enhancement or suggestion label (depending if it's new or existing functionality), to clearly differentiate it from other types of contributions
2. **Rationale** that describes why would this suggestion be considered.
3. **Use Cases** that describes how would you use this suggested functionality.

## Code Contributions
Code contribution refers to merge  (MR): take the code, change it and request merge to this repository. You can do this for any kinds of contributions, provided that you raised the issue first. Any code that you contributed to this repo will have the same license as the repository, so make sure that any libraries you use conforms to the repository's license.

Code contributions are a great way to contribute to the code directly, and eventually become the repository's developer.

### Preparations
Before you start, make sure that you have submitted an issue (bug or suggestion) before committing an MR. To associate the MR with the issue, link the issue in your MR's description. If your MR is not associated with any issues, it will be rejected.

0. (Optional) Fork the repository.
1. Clone the repository. 
2. Add a new branch based on develop branch (or master if develop doesn't exist)
3. In the folder where you cloned the repository, do an install: `npm install`
4. Change the code
5. Add a new test suite in the `test` folder and add tests to your code.
6. Run `npm test`. Make sure that the test succeeds, and the coverage displayed (in All files, % Stmts column) is equal to or higher than the repo's current coverage
7. Push the changes

### Before Submitting a Merge Request
- Each of your commits will have a pipeline associated with it. It should pass before submitting.
- Make sure to also commit the AUTHORS.md file (create one if it doesn't exist) and add a line for your name and username in this format (don't worry about formatting; it will be fixed later. Remember to replace the text in square brackets):

    \- \[Your name\] (\[@username\])

### Merge Request Guidelines
Follow the same guideline as the [bug reporting guidelines](#Bug-Report-Guidelines), with one difference: You don't need to prepend the title with anything; that is already done on the underlying issue.